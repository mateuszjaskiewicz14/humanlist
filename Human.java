import java.util.Objects;

public class Human {
    private String secendName;
    private String name;
    private int age;

    public Human(String secendName,String name,int age){
        this.name=name;
        this.secendName =secendName;
        this.age=age;
    }

    public String getSecendName() {
        return secendName;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return age == human.age &&
                Objects.equals(secendName, human.secendName) &&
                Objects.equals(name, human.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(secendName, name, age);
    }
}
